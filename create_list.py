"""
Create list of files in given directory and write it to the file in UTF-8 encoding
"""

import sys
import codecs
import os

root_dir = sys.argv[1]

script_location = os.path.dirname(os.path.abspath(__file__))
internal_list_file = os.path.join(script_location, "list.txt")

system_encoding = sys.getfilesystemencoding()  # OS
print sys.getfilesystemencoding()
print sys.getdefaultencoding()

dcs_installation = sys.argv[1]

dcs_installation_list = []
for root, dirs, files in os.walk(unicode(dcs_installation)):
    for file in files:
        print os.path.join(root, file)
        full_file_path = os.path.join(root, file)
        relative_file_path = full_file_path.replace(dcs_installation, "")
        if relative_file_path[0] == '\\':
            relative_file_path = relative_file_path[1:]
        dcs_installation_list.append("%s\n" % relative_file_path)

with codecs.open(internal_list_file, 'w') as files_list:
    for line in dcs_installation_list:
        files_list.write(unicode(line))
