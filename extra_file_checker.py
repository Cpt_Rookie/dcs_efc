"""
Goes through the directory structure given as argument as compares every file in it with internal list of files.
Internal list contains all files that are present in default DCS installation
"""

import os
import sys

script_location = os.path.dirname(os.path.abspath(__file__))
internal_list_file = os.path.join(script_location, "list.txt")

if len(sys.argv) != 2:
    print "Usage: extra_file_checker.py dcs_install_directory"
    print "Example: extra_file_checker.py C:\dcs"
    sys.exit(1)

# list of files that DCS is installing by default
internal_list = []

print "Loading default file list..."
with open(internal_list_file) as internal_list_data:
    for line in internal_list_data:
        internal_list.append(line.strip())

# list of all files that are in user's installation directory
dcs_installation = sys.argv[1]
if dcs_installation[len(dcs_installation) - 1] == '"':
    dcs_installation = dcs_installation[:-1]

dcs_installation_list = []

for root, dirs, files in os.walk(dcs_installation):
    # files
    for file in files:
        full_file_path = os.path.join(root, file)
        relative_file_path = full_file_path.replace(dcs_installation, "")
        if relative_file_path[0] == '\\':
            relative_file_path = relative_file_path[1:]
        dcs_installation_list.append(relative_file_path)

    # dirs
    for dir in dirs:
        full_file_path = os.path.join(root, dir)
        relative_file_path = full_file_path.replace(dcs_installation, "")
        if relative_file_path[0] == '\\':
            relative_file_path = relative_file_path[1:]
        dcs_installation_list.append(relative_file_path)


extra_files_and_dirs = []
print "Checking contents of %s against %s..." % (dcs_installation, internal_list_file)
for installation_file in dcs_installation_list:
    if installation_file not in internal_list:
        extra_file = os.path.join(dcs_installation, installation_file)
        print "Extra: %s" % extra_file
        extra_files_and_dirs.append(extra_file)

results_file_name = os.path.join(script_location, "results.txt")
results_file = open (results_file_name, 'w')

results_file.write("Extra files or directories:\n")
for extra_file in extra_files_and_dirs:
    results_file.write("%s\n" % extra_file)


print "\nCheck finished, results are stored in %s" % results_file_name
print "Note that you can ignore files with non-ascii characters (MissionEditor\\data\\images\\Countries\\Spain\\Troops\\10XX escuadrilla.png)"

